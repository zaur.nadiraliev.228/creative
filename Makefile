psalm:
	vendor/bin/psalm --show-info=false --output-format=phpstorm src

psalm-clear-cache:
	vendor/bin/psalm --clear-cache

cs-fix:
	vendor/bin/php-cs-fixer fix src

clear-cache:
	docker-compose exec app php bin/console c:c

composer-install:
	docker-compose exec app composer install

setup-docker:
	docker-compose up -d --build

setup-migrations:
	docker-compose exec app php bin/console d:m:migrate --no-interaction

setup-movies-data:
	docker-compose exec app php bin/console fetch:movies

setup-all: setup-docker composer-install setup-migrations setup-movies-data
