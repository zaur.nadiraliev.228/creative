import { MovieOutput, UserOutput } from "../utils/types";
import {
	Box,
	Button, CircularProgress,
	Drawer,
	DrawerBody,
	DrawerCloseButton,
	DrawerContent,
	DrawerFooter,
	DrawerHeader,
	DrawerOverlay,
	Image,
	useDisclosure,
} from "@chakra-ui/react"
import { useCallback, useEffect, useState } from "react";
import { api } from "../utils/api";
import { userStorage } from "../utils/userStorage";
import Login from "./Login";
import { dateToString } from "../utils/dateUtils";

type Props = {
	movie: MovieOutput,
	onClose: () => void,
}

export default function OneMovie({ movie, onClose }: Props) {

	const { isOpen, onOpen, onClose: onCloseDialog } = useDisclosure();
	const [user, setUser] = useState(userStorage.getUser());
	const [isMovieLiked, setIsMovieLiked] = useState(false);
	const [isLoading, setIsLoading] = useState(false);

	const onUserChanged = useCallback((user: UserOutput) => {
		setUser(user);
		userStorage.storeUser(user);

		onCloseDialog();
	}, [user]);

	const likeMovie = useCallback(() => {
		if (!userStorage.getUser()) {
			onOpen();
			return;
		}

		setIsLoading(true);
		api.likeMovie(movie.id).then(checkIsLiked).finally(() => setIsLoading(false));
	}, [movie, onOpen, setIsLoading]);

	const unlikeMovie = useCallback(() => {
		setIsLoading(true);
		api.unlikeMovie(movie.id).then(checkIsLiked).finally(() => setIsLoading(false));
	}, [movie, setIsLoading]);

	const checkIsLiked = useCallback(() => {
		api.isMovieLiked(movie.id)
			.then(({ isLiked }) => setIsMovieLiked(isLiked))
			.catch(console.log);
	}, [movie]);

	useEffect(() => {
		user && checkIsLiked();
	}, [user, movie]);

	return (
		<>
			<Login isOpen={isOpen} onOpen={onOpen} onClose={onCloseDialog} onUserChanged={onUserChanged} />

			<Drawer
				isOpen={true}
				placement="right"
				onClose={onClose}
			>
				<DrawerOverlay />
				<DrawerContent>
					<DrawerCloseButton />
					<DrawerHeader mt={8}>{movie.title}</DrawerHeader>

					<DrawerBody>
						<Image src={movie.image!} mb={5} />
						<Box as="div">{movie.description}</Box>
						<Box as="div" mt={5}>Публикация: {dateToString(movie.publicationDate)}</Box>
					</DrawerBody>

					<DrawerFooter>
						{isLoading && <Button colorScheme="blue"><CircularProgress isIndeterminate size="24px" color="teal" /></Button>}
						{!isLoading && !isMovieLiked && <Button colorScheme="blue" onClick={likeMovie}>Нравится</Button>}
						{!isLoading && isMovieLiked && <Button colorScheme="blue" onClick={unlikeMovie}>Больше не нравится</Button>}
					</DrawerFooter>
				</DrawerContent>
			</Drawer>
		</>
	);
};
