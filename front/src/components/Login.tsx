import {
	Alert,
	AlertIcon,
	Box,
	Button,
	CircularProgress,
	FormControl,
	FormLabel,
	Input,
	InputGroup,
	InputRightElement,
	Modal,
	ModalBody,
	ModalCloseButton,
	ModalContent,
	ModalFooter,
	ModalOverlay,
	Tab,
	TabList,
	TabPanel,
	TabPanels,
	Tabs
} from "@chakra-ui/react";
import { useCallback, useState } from "react";
import { ViewIcon, ViewOffIcon } from "@chakra-ui/icons";
import { api } from "../utils/api";
import { UserOutput } from "../utils/types";

type Props = {
	isOpen: boolean,
	onOpen: () => void,
	onClose: () => void,
	onUserChanged: (user: UserOutput) => void
};

export default function Login({ isOpen, onClose, onUserChanged }: Props) {
	const [isLoading, setIsLoading] = useState(false);
	const [showPassword, setShowPassword] = useState(false);

	const [name, setName] = useState('');
	const [login, setLogin] = useState('');
	const [logInPassword, setLogInPassword] = useState('');
	const [password, setPassword] = useState('');
	const [email, setEmail] = useState('');
	const [error, setError] = useState<null | string>(null);

	const togglePasswordVisibility = useCallback(() => setShowPassword(!showPassword), [showPassword, setShowPassword]);

	const onLogIn = async () => {
		if (isLoading) {
			return;
		}

		setIsLoading(true);
		setError(null);

		try {
			const user = await api.login({
				username: login,
				password: logInPassword
			});

			onUserChanged(user);
		} catch (e) {
			setError(String(e));
		} finally {
			setIsLoading(false);
		}
	};

	const onRegister = async () => {
		console.log('reg')

		if (isLoading) {
			return;
		}

		setIsLoading(true);
		setError(null);

		try {
			const { isUnique } = await api.checkLoginUniqueness(login);

			if (!isUnique) {
				setError('Login is busy!');
				return;
			}

			const { isUnique: isUniqueEmail } = await api.checkEmailUniqueness(email);

			if (!isUniqueEmail) {
				setError('Email is busy!');
				return;
			}

			const user = await api.register({
				login,
				email,
				password,
				name,
			});

			onUserChanged(user);

		} catch (e) {
			setError(String(e));
		} finally {
			setIsLoading(false);
		}
	};

	return (
		<Modal isOpen={isOpen} onClose={onClose}>
			<ModalOverlay />
			<ModalContent>

				{error
				&& <Alert status="error">
            <AlertIcon />
						{error}
        	</Alert>
				}

				<Tabs isFitted variant="enclosed">
					<TabList mb="1em" mt={15}>
						<Tab>Войти</Tab>
						<Tab>Зарегистрироваться</Tab>
					</TabList>
					<TabPanels>
						<TabPanel>
							<ModalBody>
								<Box my={4} textAlign="left">
									<form>
										<FormControl isRequired>
											<FormLabel>Login</FormLabel>
											<Input
												type="text"
												placeholder="foobar"
												size="lg"
												onChange={event => setLogin(event.currentTarget.value)}
											/>
										</FormControl>
										<FormControl isRequired mt={6}>
											<FormLabel>Password</FormLabel>
											<InputGroup>
												<Input
													type={showPassword ? 'text' : 'password'}
													placeholder="*******"
													size="lg"
													onChange={event => setLogInPassword(event.currentTarget.value)}
												/>
												<InputRightElement width="3rem">
													<Button
														h="1.5rem"
														size="sm"
														onClick={togglePasswordVisibility}
													>
														{showPassword ? <ViewOffIcon /> : <ViewIcon />}
													</Button>
												</InputRightElement>
											</InputGroup>
										</FormControl>
									</form>
								</Box>
							</ModalBody>

							<ModalFooter>
								<Button variant="ghost" onClick={onLogIn}>
									{isLoading
										? <CircularProgress isIndeterminate size="24px" color="teal" />
										: 'Войти'
									}
								</Button>
							</ModalFooter>
						</TabPanel>

						<TabPanel>
							<ModalBody>
								<Box my={4} textAlign="left">
									<form>
										<FormControl isRequired>
											<FormLabel>Name</FormLabel>
											<Input
												type="text"
												placeholder="Foobar"
												size="lg"
												onChange={event => setName(event.currentTarget.value)}
											/>
										</FormControl>
										<FormControl isRequired>
											<FormLabel>Email</FormLabel>
											<Input
												type="email"
												placeholder="test@test.ru"
												size="lg"
												onChange={event => setEmail(event.currentTarget.value)}
											/>
										</FormControl>
										<FormControl isRequired>
											<FormLabel>Login</FormLabel>
											<Input
												type="text"
												placeholder="foobar"
												size="lg"
												onChange={event => setLogin(event.currentTarget.value)}
											/>
										</FormControl>
										<FormControl isRequired mt={6}>
											<FormLabel>Password</FormLabel>
											<InputGroup>
												<Input
													type={showPassword ? 'text' : 'password'}
													placeholder="*******"
													size="lg"
													onChange={event => setPassword(event.currentTarget.value)}
												/>
												<InputRightElement width="3rem">
													<Button
														h="1.5rem"
														size="sm"
														onClick={togglePasswordVisibility}
													>
														{showPassword ? <ViewOffIcon /> : <ViewIcon />}
													</Button>
												</InputRightElement>
											</InputGroup>
										</FormControl>
									</form>
								</Box>
							</ModalBody>

							<ModalFooter>
								<Button variant="ghost" onClick={onRegister}>
									{isLoading
										? <CircularProgress isIndeterminate size="24px" color="teal" />
										: 'Зарегистрироваться'
									}
								</Button>
							</ModalFooter>
						</TabPanel>
					</TabPanels>
				</Tabs>

				<ModalCloseButton />
			</ModalContent>
		</Modal>
	)
};
