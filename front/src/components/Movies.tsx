import { useCallback, useEffect, useState } from "react";
import { MovieOutput, Pagination } from "../utils/types";
import { api } from "../utils/api";
import {
	Box,
	Grid,
	Image,
	NumberDecrementStepper,
	NumberIncrementStepper,
	NumberInput,
	NumberInputField,
	NumberInputStepper,
	Stack
} from "@chakra-ui/react";
import { ExternalLinkIcon, LinkIcon } from "@chakra-ui/icons";
import { dateToString } from "../utils/dateUtils";

type Props = {
	onMovieSelected: (movie: MovieOutput) => void
};

export default function Movies({ onMovieSelected }: Props) {
	const [totalCount, setTotalCount] = useState(0);
	const [movies, setMovies] = useState<Array<MovieOutput>>([]);
	const [pagination, setPagination] = useState<Pagination>({ limit: 10, offset: 0 });

	const setLimit = useCallback((limit: number) => setPagination({ ...pagination, limit }), [pagination]);

	useEffect(() => {
		api.findAllMovies(pagination)
			.then(it => {
				setTotalCount(it.totalCount);
				setMovies(it.data);
			})
			.catch(console.log)
	}, [pagination]);

	const renderCard = useCallback((movie: MovieOutput) => (
		<Box maxW="sm" borderWidth="1px" borderRadius="lg" overflow="hidden" width={160}>
			<Image src={movie.image!} />

			<Box p="6">
				<Box d="flex" alignItems="baseline">
					<Box
						color="gray.500"
						fontWeight="semibold"
						letterSpacing="wide"
						fontSize="xs"
						textTransform="uppercase"
						ml="2"
					>
					</Box>
				</Box>

				<Box
					mt="1"
					fontWeight="semibold"
					as="h4"
					lineHeight="tight"
					isTruncated
				>
				</Box>

				<Box>
					<Box as="span" color="gray.600" fontSize="sm">
						{movie.title}
					</Box>

					<Box>
						<LinkIcon
							style={{ cursor: 'pointer' }}
							m={3}
							onClick={() => onMovieSelected(movie)}
						/>

						<a href={movie.link} target="_blank">
							<ExternalLinkIcon m={3} />
						</a>
					</Box>
					<Box as="span">{dateToString(movie.publicationDate)}</Box>
				</Box>

				<Box d="flex" mt="2" alignItems="center">
					<Box as="span" ml="2" color="gray.600" fontSize="sm">
						{movie.likesCount} likes
					</Box>
				</Box>
			</Box>
		</Box>
	), []);

	return (
		<div style={{ padding: '30px' }}>
			{movies.length &&
      <Box as="div" mb={5}>
          <Stack direction="row">
              <span>Всего: {totalCount}, на странице</span>
              <NumberInput size="sm" defaultValue={10} min={1} max={totalCount} onChange={val => setLimit(Number(val))}>
                  <NumberInputField />
                  <NumberInputStepper>
                      <NumberIncrementStepper />
                      <NumberDecrementStepper />
                  </NumberInputStepper>
              </NumberInput>
          </Stack>
      </Box>}

			<Grid templateColumns="repeat(6, 1fr)" gap={4}>
				{movies.map(renderCard)}
			</Grid>

			{movies.length === 0 && <h1>No content</h1>}
		</div>
	);
};
