import React, { useState } from 'react';
import { BrowserRouter as Router} from 'react-router-dom';
import { Button, ChakraProvider } from '@chakra-ui/react'
import Movies from "./components/Movies";
import { MovieOutput } from "./utils/types";
import OneMovie from "./components/OneMovie";
import { userStorage } from "./utils/userStorage";

export default function App() {
	const [movie, setMovie] = useState<null | MovieOutput>(null);

	const user = userStorage.getUser();

	const logout = () => {
		userStorage.clear();
		window.location.reload();
	};

	return (
		<Router>
			<ChakraProvider>
				{user && <Button onClick={logout} type='reset' mt={5} ml={5}>Выйти</Button>}
				<Movies onMovieSelected={setMovie}/>
				{movie && <OneMovie movie={movie} onClose={() => setMovie(null)}/>}
			</ChakraProvider>
		</Router>
	);
}
