import axios from 'axios';
import { userStorage } from "./userStorage";

const API_HOST = 'http://localhost:8090'; // todo: this should not be hardcoded

const regex = new RegExp('^/');

export function enableInterceptors() {
	axios.interceptors.request.use(async config => {
		const user = userStorage.getUser();

		if (user) {
			config.headers['Authorization'] = `Bearer ${user.token}`;
		}

		config.url = `${API_HOST}/${config.url?.replace(regex, '')}`;

		return config;
	});


	axios.interceptors.response.use(it => it, error => {
		if (hasResponse(error)) {
			if (error.response.status === 401) {
				if (!userStorage.getUser()) {
					return error;
				}

				userStorage.clear();
				window.location.reload();

				return error;
			}
		}

		return error;
	});
}

function hasResponse(it: any): it is { response: { data: any, status: number } } {
	return it?.response?.status && typeof it.response.status === 'number';
}

