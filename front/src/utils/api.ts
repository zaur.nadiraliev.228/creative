import {
	CreateUserInput,
	IsUniqueOutput,
	LoginInput,
	MovieOutput,
	PaginatedResult,
	Pagination,
	UserOutput
} from "./types";
import axios from 'axios';

export const api = {
	async login(input: LoginInput): Promise<UserOutput> {
		const { data } = await axios.post('/login', input);

		return throwErrorIfUndefined(data);
	},

	async register(input: CreateUserInput): Promise<UserOutput> {
		const { data } = await axios.post('/users', input);

		return throwErrorIfUndefined(data);
	},

	async checkLoginUniqueness(login: string): Promise<IsUniqueOutput> {
		const { data } = await axios.get(`/users/login/check?login=${login}`);

		return throwErrorIfUndefined(data);
	},

	async checkEmailUniqueness(email: string): Promise<IsUniqueOutput> {
		const { data } = await axios.get(`/users/email/check?email=${email}`);

		return throwErrorIfUndefined(data);
	},

	async findAllMovies({ limit, offset }: Pagination): Promise<PaginatedResult<MovieOutput>> {
		const { data } = await axios.get(`/movies?limit=${limit}&offset=${offset}`);

		return throwErrorIfUndefined(data);
	},

	async likeMovie(movieId: string): Promise<void> {
		const { data } = await axios.patch(`/movies/${movieId}/like`);

		return throwErrorIfUndefined(data);
	},

	async unlikeMovie(movieId: string): Promise<void> {
		const { data } = await axios.patch(`/movies/${movieId}/unlike`);

		return throwErrorIfUndefined(data);
	},

	async isMovieLiked(movieId: string): Promise<{ isLiked: boolean }> {
		const { data } = await axios.get(`/movies/${movieId}/is-liked`);

		return throwErrorIfUndefined(data);
	}
}

function throwErrorIfUndefined<T>(val: T): T {
	if (undefined === val) {
		throw new Error('No response from server');
	}

	return val;
}
