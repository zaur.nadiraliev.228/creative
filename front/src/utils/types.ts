export type LoginInput = {
	username: string,
	password: string,
}

export type CreateUserInput = {
	name: string,
	login: string,
	email: string,
	password: string,
}

export type UserOutput = Omit<CreateUserInput, 'password'> & { id: string, token: string };

export type IsUniqueOutput = { isUnique: boolean };

export type MovieOutput = {
	id: string,
	title: string,
	description: string,
	link: string,
	publicationDate: string,
	image: null | string,
	likesCount: number,
}

export type Pagination = { limit: number, offset: number };
export type PaginatedResult<T> = { totalCount: number, data: Array<T> }
