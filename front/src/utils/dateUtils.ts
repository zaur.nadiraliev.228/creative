
import * as dateFns from 'date-fns';

const DEFAULT_FORMAT = 'dd//MM//yyyy HH:mm';

export function dateToString(dateToParse: string, format?: string): string {
	return dateFns.format(dateFns.parseISO(dateToParse), format ?? DEFAULT_FORMAT);
}
