import { UserOutput } from "./types";
import * as dateFns from 'date-fns';

const userKey = 'user:current';

function storeUser(data: UserOutput): void {
	localStorage.setItem(userKey, JSON.stringify(data));
}

function getUser(): undefined | UserOutput {
	const data = localStorage.getItem(userKey);

	if (!data) {
		return;
	}

	const user = JSON.parse(data);

	if (isAccessTokenExpired(user.token)) {
		clear();

		return;
	}

	return user;
}

function isAccessTokenExpired(accessToken: string): boolean {
	const [, payload] = accessToken.split('.');

	if (!payload) {
		return true;
	}

	const { exp } = JSON.parse(atob(payload)) as { exp: any };

	if (!exp || typeof exp !== 'number') {
		return true;
	}

	return dateFns.isPast(dateFns.fromUnixTime(exp));
}

function clear():  void {
	localStorage.removeItem(userKey);
}

export const userStorage = { storeUser, getUser, clear };
