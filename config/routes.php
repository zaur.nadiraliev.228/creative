<?php

declare(strict_types=1);

use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return function (RoutingConfigurator $configurator) {
    $configurator
        ->import('../src/Controller/**/*', 'annotation');

    $configurator->add('api_login_check', '/login');
};
