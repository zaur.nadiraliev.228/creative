<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return function (ContainerConfigurator $configurator) {
    $configurator
        ->extension('doctrine', [
            'orm' => [
                'auto_mapping' => true,
                'auto_generate_proxy_classes' => true,
                'naming_strategy' => 'doctrine.orm.naming_strategy.underscore',
                'mappings' => [
                    'App\Entity' => [
                        'type' => 'attribute',
                        'dir' => '%kernel.project_dir%/src/Entity',
                        'is_bundle' => false,
                        'prefix' => 'App\Entity',
                        'alias' => 'App',
                    ],
                ],
            ],
            'dbal' => [
                'charset' => 'utf8',
                'driver' => 'pdo_pgsql',
                'user' => '%env(resolve:PSQL_USER)%',
                'password' => '%env(resolve:PSQL_PASS)%',
                'dbname' => '%env(resolve:PSQL_DATABASE)%',
                'host' => '%env(resolve:PSQL_HOST)%',
                'port' => '%env(resolve:PSQL_PORT)%',
            ],
        ]);

    $configurator
        ->extension('doctrine_migrations', [
            'migrations_paths' => [
                'App\Migrations' => '%kernel.project_dir%/src/Migrations',
            ],
        ]);
};
