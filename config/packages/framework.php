<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return function (ContainerConfigurator $configurator) {
    $configurator
        ->extension('framework', [
            'secret' => '%env(APP_SECRET)%',
            'session' => [
                'handler_id' => null,
            ],
            'php_errors' => ['log' => true],
            'router' => [
                'strict_requirements' => '~',
                'utf8' => true,
            ],
            'validation' => ['enable_annotations' => true],
        ]);
};
