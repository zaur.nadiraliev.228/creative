<?php

declare(strict_types=1);

use App\Application\Security\UserAdapter;
use App\Application\Security\UserProvider;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Guard\JWTTokenAuthenticator;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationFailureHandler;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return function (ContainerConfigurator $configurator) {
    $services = $configurator->services();

    $services->defaults()
        ->autoconfigure()
        ->autowire();

    $services->set(UserProvider::class);
    $services->set(JWTTokenAuthenticator::class);

    $configurator->extension('security', [
        'enable_authenticator_manager' => true,

        'password_hashers' => [
            UserAdapter::class => ['algorithm' => 'bcrypt'],
        ],

        'providers' => [
            UserProvider::class => ['id' => UserProvider::class],
        ],

        'firewalls' => [
            'login' => [
                'pattern' => '^/login',
                'stateless' => true,
                'json_login' => [
                    'check_path' => '/login',
                    'success_handler' => AuthenticationSuccessHandler::class,
                    'failure_handler' => AuthenticationFailureHandler::class,
                ],
            ],

            'authorized' => [
                'pattern' => '^/(?!login|health|users)',
                'stateless' => true,
                'provider' => UserProvider::class,
                'guard' => [
                    'authenticators' => [JWTTokenAuthenticator::class],
                ],
            ],
        ],
    ]);
};
