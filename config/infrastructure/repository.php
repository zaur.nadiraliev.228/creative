<?php

declare(strict_types=1);

use App\Entity\Movie\MovieRepositoryInterface;
use App\Entity\User\UserRepositoryInterface;
use App\Infrastructure\Doctrine\Repository\MovieRepository;
use App\Infrastructure\Doctrine\Repository\UserRepository;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return function (ContainerConfigurator $configurator) {
    $services = $configurator->services();

    $services->defaults()
        ->autowire()
        ->autoconfigure();

    $registerRepository = static fn (string $interface, string $impl) => $services->set($impl)->alias($interface, $impl);

    $registerRepository(interface: MovieRepositoryInterface::class, impl: MovieRepository::class);
    $registerRepository(interface: UserRepositoryInterface::class, impl: UserRepository::class);
};
