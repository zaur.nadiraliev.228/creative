<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

return function (ContainerConfigurator $configurator) {
    $services = $configurator->services();

    $services->defaults()
        ->autoconfigure()
        ->autowire();

    /**
     * @var list<class-string<NormalizerInterface>> $normalizers
     */
    $normalizers = [];

    foreach ($normalizers as $normalizer) {
        $services->set($normalizer)->tag('serializer.normalizer');
    }
};
