<?php

declare(strict_types=1);

use App\Infrastructure\Service\Movie\MovieSourceRssService;
use App\Infrastructure\Service\Movie\MovieSourceServiceInterface;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return function (ContainerConfigurator $configurator) {
    $configurator->import('repository.php');
    $configurator->import('listener.php');
    $configurator->import('serializer.php');
    $configurator->import('resolver.php');

    $configurator->services()
        ->set(MovieSourceServiceInterface::class, MovieSourceRssService::class)
        ->arg('$source', '%env(TRAILERS_RSS_SOURCE)%')
        ->autowire();
};
