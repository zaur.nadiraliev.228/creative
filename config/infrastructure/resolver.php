<?php

declare(strict_types=1);

use App\Infrastructure\Resolving\FromBodyArgumentResolver;
use App\Infrastructure\Resolving\FromQueryArgumentResolver;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

const RESOLVER_TAG = 'controller.argument_value_resolver';

return function (ContainerConfigurator $configurator) {
    $services = $configurator->services();

    $services->defaults()
        ->autoconfigure()
        ->autowire();

    $services->set(FromBodyArgumentResolver::class)->tag(RESOLVER_TAG);
    $services->set(FromQueryArgumentResolver::class)->tag(RESOLVER_TAG);
};
