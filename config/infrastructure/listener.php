<?php

declare(strict_types=1);

use App\Infrastructure\Doctrine\FixPostgreSQLDefaultSchemaListener;
use App\Infrastructure\HttpListener\AuthenticationSuccessListener;
use App\Infrastructure\HttpListener\InputValidationListener;
use App\Infrastructure\HttpListener\JsonResponseListener;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return function (ContainerConfigurator $configurator) {
    $services = $configurator->services();

    $services->defaults()
        ->autoconfigure()
        ->autowire();

    $services
        ->set(FixPostgreSQLDefaultSchemaListener::class)
        ->tag('doctrine.event_listener', ['event' => 'postGenerateSchema']);

    $services
        ->set(JsonResponseListener::class)
        ->tag('kernel.event_listener', [
            'event' => 'kernel.view',
            'method' => 'handle',
        ]);

    $services
        ->set(AuthenticationSuccessListener::class)
        ->tag('kernel.event_listener', [
            'event' => 'lexik_jwt_authentication.on_authentication_success',
            'method' => 'handle',
        ]);

    $services
        ->set(InputValidationListener::class)
        ->tag('kernel.event_listener', [
            'event' => 'kernel.controller_arguments',
            'method' => 'handle',
        ]);
};
