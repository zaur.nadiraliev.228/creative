<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return function (ContainerConfigurator $configurator) {
    $services = $configurator->services();

    $services->defaults()
        ->autoconfigure()
        ->autowire();

    $services->load('App\\Application\\CQS\\', '%kernel.project_dir%/src/Application/CQS/**/*{Command,Query}.php');
};
