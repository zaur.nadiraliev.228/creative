<?php

declare(strict_types=1);

use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return function (ContainerConfigurator $configurator) {
    $subModules = [
        'application',
        'infrastructure',
    ];

    foreach ($subModules as $module) {
        $configurator->import("{$module}/services.php");
    }
};
