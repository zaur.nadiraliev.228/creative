<?php

$finder = PhpCsFixer\Finder::create()
    ->ignoreDotFiles(false)
    ->ignoreVCS(true)
    ->exclude([
        'vendor',
    ])
    ->in(__DIR__ . '/src')
;
$rules = [
    '@Symfony' => true,
    'array_indentation' => true,
    'compact_nullable_typehint' => true,
    'multiline_comment_opening_closing' => false,
    'new_with_braces' => false,
    //'phpdoc_inline_tag' => false,
    'phpdoc_to_comment' => false,
    'single_import_per_statement' => false,
    'concat_space' => ['spacing' => 'one'],
    'array_syntax' => ['syntax' => 'short'],
    'no_superfluous_phpdoc_tags' => false,
    'blank_line_after_opening_tag' => true,
    'linebreak_after_opening_tag' => false,
    'global_namespace_import' => [
        'import_classes' => true,
        'import_constants' => true,
        'import_functions' => true,
    ],
    'phpdoc_separation' => true,
    'yoda_style' => false,
];

return (new PhpCsFixer\Config())->setRules($rules)->setFinder($finder);
