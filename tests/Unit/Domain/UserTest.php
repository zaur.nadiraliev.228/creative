<?php

declare(strict_types=1);

namespace Tests\Unit\Domain;

use App\Entity\Movie\Movie;
use App\Entity\User\User;
use PHPUnit\Framework\TestCase;

final class UserTest extends TestCase
{
    public function testLikeAndUnlikeMovieByUser()
    {
        $user = self::createFakeUser();
        $movie = self::createFakeMovie();

        self::assertFalse($movie->isUserAlreadyLiked($user));

        $user->likeMovie($movie);

        self::assertTrue($movie->isUserAlreadyLiked($user));

        $user->unlikeMovie($movie);

        self::assertFalse($movie->isUserAlreadyLiked($user));
    }

    private static function createFakeUser(): User
    {
        return new User(
            name: 'test',
            login: 'test',
            email: 'test@test.ru',
            password: '*****',
        );
    }

    private static function createFakeMovie(): Movie
    {
        return new Movie(
            title: 'test movie',
            link: 'https://ya.ru',
            description: 'test movie',
            image: 'https://ya.ru'
        );
    }
}
