<?php

declare(strict_types=1);

namespace Tests\Application\CQS\User;

use App\Application\CQS\User\CreateUserCommand;
use App\Application\CQS\User\CreateUserInput;
use App\Entity\User\User;
use App\Entity\User\UserRepositoryInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;

final class CreateUserCommandTest extends TestCase
{
    /** @var UserRepositoryInterface & MockObject */
    private $userRepository;

    /** @var PasswordHasherInterface & MockObject */
    private $passwordHasher;

    private CreateUserCommand $command;

    protected function setUp(): void
    {
        parent::setUp();

        $this->userRepository = $this->createMock(UserRepositoryInterface::class);

        $passwordHasherFactory = $this->createMock(PasswordHasherFactoryInterface::class);
        $this->passwordHasher = $this->createMock(PasswordHasherInterface::class);

        $passwordHasherFactory->method('getPasswordHasher')->willReturn($this->passwordHasher);

        $this->command = new CreateUserCommand($this->userRepository, $passwordHasherFactory);
    }

    public function testCreateUserWhenLoginIsBusy()
    {
        $busyLogin = 'test';

        $this->userRepository->method('findByLogin')->willReturn(
            new User(name: 'any', login: 'any', email: 'any', password: 'any'),
        );

        $this->expectException(RuntimeException::class);

        ($this->command)(new CreateUserInput(name: 'fake', login: $busyLogin, email: 'fake@test.ru', password: 'fake'));
    }

    public function testCreateUserWhenEmailIsBusy()
    {
        $busyEmail = 'test@test.ru';

        $this->userRepository->method('findByLogin')->willReturn(null);

        $this->userRepository->method('findByEmail')->willReturn(
            new User(name: 'any', login: 'any', email: 'any', password: 'any'),
        );

        $this->expectException(RuntimeException::class);

        ($this->command)(new CreateUserInput(name: 'fake', login: 'login', email: $busyEmail, password: 'fake'));
    }

    public function testCreateUserSuccessfully()
    {
        $expected = [
            'name' => 'test',
            'login' => 'test',
            'email' => 'test@test.ru',
            'password' => 'secret_password_hash',
        ];

        $this->userRepository->method('findByLogin')->willReturn(null);
        $this->passwordHasher->method('hash')->willReturn($expected['password']);

        $this->userRepository->expects(self::once())->method('save');

        $createdUser = ($this->command)(new CreateUserInput(
            name: $expected['name'],
            login: $expected['login'],
            email: $expected['email'],
            password: 'plain_password',
        ));

        self::assertEquals($expected['name'], $createdUser->getName());
        self::assertEquals($expected['login'], $createdUser->getLogin());
        self::assertEquals($expected['email'], $createdUser->getEmail());
        self::assertEquals($expected['password'], $createdUser->getPassword());
    }
}
