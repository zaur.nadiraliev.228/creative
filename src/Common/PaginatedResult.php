<?php

declare(strict_types=1);

namespace App\Common;

use function array_map;

/**
 * @template T
 *
 * @psalm-immutable
 */
final class PaginatedResult
{
    /**
     * @param int     $totalCount
     * @param list<T> $data
     */
    public function __construct(
        public int $totalCount,
        public array $data,
    ) {
    }

    /**
     * @template TMapped
     *
     * @param pure-callable(T): TMapped $mapper
     *
     * @return self<TMapped>
     */
    public function map(callable $mapper): self
    {
        return new self(
            $this->totalCount,
            array_map($mapper, $this->data),
        );
    }
}
