<?php

declare(strict_types=1);

namespace App\Common;

/**
 * @psalm-immutable
 */
final class Pagination
{
    public function __construct(
        public ?int $limit = null,
        public ?int $offset = null,
    ) {
    }
}
