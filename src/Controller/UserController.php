<?php

declare(strict_types=1);

namespace App\Controller;

use App\Application\CQS\User\CreateUserCommand;
use App\Application\CQS\User\CreateUserInput;
use App\Application\CQS\User\FindUserByEmailQuery;
use App\Application\CQS\User\FindUserByLoginQuery;
use App\Application\CQS\User\UserOutput;
use App\Application\Metadata\ResolveFromBody;
use App\Application\Metadata\ResolveFromQuery;
use App\Application\Security\UserAdapter;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

final class UserController
{
    #[Route(path: '/users', methods: ['POST'])]
    public function create(
        CreateUserCommand $createUser,
        JWTTokenManagerInterface $tokenManager,
        #[ResolveFromBody] CreateUserInput $input
    ): UserOutput {
        $user = $createUser($input);

        $accessToken = $tokenManager->create(new UserAdapter($user));

        return UserOutput::createFromUser($user, $accessToken);
    }

    #[Route(path: '/users/login/check', methods: ['GET'])]
    public function checkLoginUniqueness(FindUserByLoginQuery $findUserByLogin, #[ResolveFromQuery] string $login): array
    {
        return [
            'isUnique' => $findUserByLogin($login) === null,
        ];
    }

    #[Route(path: '/users/email/check', methods: ['GET'])]
    public function checkEmailUniqueness(FindUserByEmailQuery $findUserByEmail, #[ResolveFromQuery] string $email): array
    {
        return [
            'isUnique' => $findUserByEmail($email) === null,
        ];
    }
}
