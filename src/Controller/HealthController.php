<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

final class HealthController
{
    #[Route(path: '/health')]
    public function health(): array
    {
        return ['status' => 'ok'];
    }
}
