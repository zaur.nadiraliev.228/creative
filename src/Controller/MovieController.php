<?php

declare(strict_types=1);

namespace App\Controller;

use App\Application\CQS\Movie\FindMoviesQuery;
use App\Application\CQS\Movie\GetOneMovieQuery;
use App\Application\CQS\Movie\MovieOutput;
use App\Application\Metadata\ResolveFromQuery;
use App\Application\Security\UserAdapter;
use App\Common\PaginatedResult;
use App\Common\Pagination;
use App\Entity\Movie\Movie;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\CurrentUser;

#[Route('/movies')]
final class MovieController
{
    public function __construct(private EntityManagerInterface $em)
    {
    }

    #[Route(path: '{movieId}', methods: ['GET'])]
    public function getOneMovie(GetOneMovieQuery $getMovieQuery, string $movieId): MovieOutput
    {
        return MovieOutput::createFromMovie($getMovieQuery($movieId));
    }

    /**
     * @return PaginatedResult<MovieOutput>
     */
    #[Route(path: '', methods: ['GET'])]
    public function getMovies(FindMoviesQuery $findMovies, #[ResolveFromQuery] Pagination $pagination): PaginatedResult
    {
        return $findMovies($pagination)->map(static fn (Movie $movie) => MovieOutput::createFromMovie($movie));
    }

    #[Route(path: '/{movieId}/like', methods: ['PATCH'])]
    public function likeMovie(GetOneMovieQuery $getMovieQuery, string $movieId, #[CurrentUser] UserAdapter $user)
    {
        $user->unwrap()->likeMovie($getMovieQuery($movieId));

        $this->em->flush(); // this should not be this way
    }

    #[Route(path: '/{movieId}/unlike', methods: ['PATCH'])]
    public function unlikeMovie(GetOneMovieQuery $getMovieQuery, string $movieId, #[CurrentUser] UserAdapter $user)
    {
        $user->unwrap()->unlikeMovie($getMovieQuery($movieId));

        $this->em->flush(); // this should not be this way
    }

    #[Route(path: '/{movieId}/is-liked', methods: ['GET'])]
    public function isMovieLiked(string $movieId, #[CurrentUser] UserAdapter $user, GetOneMovieQuery $getMovie)
    {
        return [
            'isLiked' => $getMovie($movieId)->isUserAlreadyLiked($user->unwrap()),
        ];
    }
}
