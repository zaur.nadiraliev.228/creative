<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Movie\Movie;
use App\Entity\Movie\MovieRepositoryInterface;
use App\Infrastructure\Service\Movie\MovieSourceServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class FetchDataCommand extends Command
{
    public function __construct(
        private LoggerInterface $logger,
        private MovieRepositoryInterface $movieRepository,
        private MovieSourceServiceInterface $moviesSourceService,
        private EntityManagerInterface $entityManager,
    ) {
        parent::__construct('fetch:movies');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->logger->info(sprintf('Start %s at %s', __CLASS__, (string) date_create()->format(DATE_ATOM)));

        $movies = $this->moviesSourceService->loadAllMovies($output);

        // todo: there should be another abstraction for transactions
        $this->entityManager->transactional(function () use ($movies) {
            $this->movieRepository->clearAll();

            foreach ($movies as $movie) {
                $movieEntity = new Movie(
                    $movie['title'],
                    $movie['link'],
                    $movie['description'],
                    $movie['pubDate'],
                    $movie['image'],
                );

                $this->movieRepository->save($movieEntity);
            }
        });

        $this->logger->info(sprintf('End %s at %s', __CLASS__, (string) date_create()->format(DATE_ATOM)));

        return 0;
    }
}
