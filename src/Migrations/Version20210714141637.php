<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210714141637 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add table movies_users';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE movies_users (movie_id UUID NOT NULL, user_id UUID NOT NULL, PRIMARY KEY(movie_id, user_id))');
        $this->addSql('CREATE INDEX IDX_6F6187E88F93B6FC ON movies_users (movie_id)');
        $this->addSql('CREATE INDEX IDX_6F6187E8A76ED395 ON movies_users (user_id)');
        $this->addSql('COMMENT ON COLUMN movies_users.movie_id IS \'(DC2Type:uuid)\'');
        $this->addSql('COMMENT ON COLUMN movies_users.user_id IS \'(DC2Type:uuid)\'');
        $this->addSql('ALTER TABLE movies_users ADD CONSTRAINT FK_6F6187E88F93B6FC FOREIGN KEY (movie_id) REFERENCES movies (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE movies_users ADD CONSTRAINT FK_6F6187E8A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE movies_users');
    }
}
