<?php

declare(strict_types=1);

namespace App\Application\Security;

use App\Entity\User\User;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @psalm-immutable
 */
final class UserAdapter implements UserInterface, PasswordAuthenticatedUserInterface
{
    public function __construct(private User $user)
    {
    }

    /**
     * @return list<string>
     */
    public function getRoles(): array
    {
        return [];
    }

    public function getPassword(): string
    {
        return $this->user->getPassword();
    }

    public function getUsername(): string
    {
        return $this->user->getLogin();
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function eraseCredentials(): void
    {
        // nothing to do here
    }

    public function getUserIdentifier(): string
    {
        return (string) $this->user->getId();
    }

    public function unwrap(): User
    {
        return $this->user;
    }
}
