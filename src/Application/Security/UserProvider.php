<?php

declare(strict_types=1);

namespace App\Application\Security;

use App\Entity\User\UserRepositoryInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

final class UserProvider implements UserProviderInterface
{
    public function __construct(private UserRepositoryInterface $userRepository)
    {
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        return $this->loadUserByIdentifier($user->getUserIdentifier());
    }

    public function supportsClass(string $class): bool
    {
        return $class === UserAdapter::class;
    }

    public function loadUserByUsername(string $username): UserInterface
    {
        $user = $this->userRepository->findByLogin($username)
            ?? throw new UserNotFoundException("User with username {$username} was not found.");

        return new UserAdapter($user);
    }

    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        $user = $this->userRepository->findByLogin($identifier)
            ?? throw new UserNotFoundException("User with login {$identifier} was not found.");

        return new UserAdapter($user);
    }
}
