<?php

declare(strict_types=1);

namespace App\Application\Metadata;

use Attribute;

#[Attribute(Attribute::TARGET_PARAMETER)]
final class ResolveFromBody
{
}
