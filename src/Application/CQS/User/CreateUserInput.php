<?php

declare(strict_types=1);

namespace App\Application\CQS\User;

use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * @psalm-immutable
 */
final class CreateUserInput
{
    public function __construct(
        #[NotBlank]
        public string $name,

        #[NotBlank]
        #[Length(min: 4)]
        public string $login,

        #[NotBlank]
        #[Email]
        public string $email,

        #[NotBlank]
        #[Length(min: 6)]
        public string $password,
    ) {
    }
}
