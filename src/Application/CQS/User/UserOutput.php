<?php

declare(strict_types=1);

namespace App\Application\CQS\User;

use App\Entity\User\User;

/**
 * @psalm-immutable
 */
final class UserOutput
{
    public function __construct(
        public string $id,
        public string $name,
        public string $login,
        public string $email,
        public string $token,
    ) {
    }

    public static function createFromUser(User $user, string $accessToken): self
    {
        return new self(
            (string) $user->getId(),
            $user->getName(),
            $user->getLogin(),
            $user->getEmail(),
            $accessToken,
        );
    }
}
