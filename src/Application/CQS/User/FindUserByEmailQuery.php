<?php

declare(strict_types=1);

namespace App\Application\CQS\User;

use App\Entity\User\User;
use App\Entity\User\UserRepositoryInterface;

final class FindUserByEmailQuery
{
    public function __construct(private UserRepositoryInterface $userRepository)
    {
    }

    public function __invoke(string $email): ?User
    {
        return $this->userRepository->findByEmail($email);
    }
}
