<?php

declare(strict_types=1);

namespace App\Application\CQS\User;

use App\Application\Security\UserAdapter;
use App\Entity\User\User;
use App\Entity\User\UserRepositoryInterface;
use RuntimeException;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;

final class CreateUserCommand
{
    public function __construct(
        private UserRepositoryInterface $userRepository,
        private PasswordHasherFactoryInterface $passwordHasherFactory,
    ) {
    }

    public function __invoke(CreateUserInput $input): User
    {
        $this->assertLoginIsUnique($input->login);
        $this->assertEmailIsUnique($input->email);

        $user = new User(
            name: $input->name,
            login: $input->login,
            email: $input->email,
            password: $input->password,
        );

        $hashedPassword = $this->passwordHasherFactory
            ->getPasswordHasher(new UserAdapter($user))
            ->hash($input->password);

        $user->setPassword($hashedPassword);

        $this->userRepository->save($user);

        return $user;
    }

    private function assertLoginIsUnique(string $login): void
    {
        $found = $this->userRepository->findByLogin($login);

        if ($found !== null) {
            throw new RuntimeException("User with login '{$login}' already exists"); // todo change exception
        }
    }

    private function assertEmailIsUnique(string $email): void
    {
        $found = $this->userRepository->findByEmail($email);

        if ($found !== null) {
            throw new RuntimeException("User with email '{$email}' already exists"); // todo change exception
        }
    }
}
