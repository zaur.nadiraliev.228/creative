<?php

declare(strict_types=1);

namespace App\Application\CQS\Movie;

use App\Entity\Movie\Movie;
use DateTimeImmutable;

/**
 * @psalm-immutable
 */
final class MovieOutput
{
    public function __construct(
        public string $id,
        public string $title,
        public string $link,
        public string $description,
        public ?string $image,
        public int $likesCount,
        public DateTimeImmutable $publicationDate,
    ) {
    }

    /**
     * @psalm-pure
     */
    public static function createFromMovie(Movie $movie): self
    {
        return new self(
            (string) $movie->getId(),
            $movie->getTitle(),
            $movie->getLink(),
            $movie->getDescription(),
            $movie->getImage(),
            $movie->getCountOfLikedUsers(),
            $movie->getPublicationDate(),
        );
    }
}
