<?php

declare(strict_types=1);

namespace App\Application\CQS\Movie;

use App\Entity\Movie\Movie;
use App\Entity\Movie\MovieRepositoryInterface;
use RuntimeException;

final class GetOneMovieQuery
{
    public function __construct(private MovieRepositoryInterface $movieRepository)
    {
    }

    public function __invoke(string $movieId): Movie
    {
        return $this->movieRepository->findById($movieId)
            ?? throw new RuntimeException("Movie with id {$movieId} not found"); // todo change exception
    }
}
