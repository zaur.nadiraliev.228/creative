<?php

declare(strict_types=1);

namespace App\Application\CQS\Movie;

use App\Common\PaginatedResult;
use App\Common\Pagination;
use App\Entity\Movie\Movie;
use App\Entity\Movie\MovieRepositoryInterface;

final class FindMoviesQuery
{
    public function __construct(private MovieRepositoryInterface $movieRepository)
    {
    }

    /**
     * @return PaginatedResult<Movie>
     */
    public function __invoke(Pagination $pagination): PaginatedResult
    {
        $movies = $this->movieRepository->findAll($pagination->limit, $pagination->offset);
        $totalCount = $this->movieRepository->countAll();

        return new PaginatedResult($totalCount, $movies);
    }
}
