<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpListener;

use Symfony\Component\HttpKernel\Event\ControllerArgumentsEvent;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class InputValidationListener
{
    public function __construct(private ValidatorInterface $validator)
    {
    }

    public function handle(ControllerArgumentsEvent $event): void
    {
        /** @psalm-suppress MixedAssignment */
        foreach ($event->getArguments() as $argument) {
            if (!$this->validator->hasMetadataFor($argument)) {
                continue;
            }

            $validationErrors = $this->validator->validate($argument);

            if ($validationErrors->count()) {
                throw new ValidationFailedException($argument, $validationErrors);
            }
        }
    }
}
