<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\Serializer\SerializerInterface;

final class JsonResponseListener
{
    public function __construct(private SerializerInterface $serializer)
    {
    }

    public function handle(ViewEvent $event): void
    {
        /** @psalm-suppress MixedAssignment */
        $controllerResult = $event->getControllerResult();

        if ($controllerResult instanceof Response) {
            return;
        }

        $response = new JsonResponse();

        $statusCode = Response::HTTP_NO_CONTENT;

        if ($controllerResult !== null) {
            $response->setJson($this->serializer->serialize($controllerResult, format: 'json'));

            $statusCode = $event->getRequest()->isMethod('POST')
                ? Response::HTTP_CREATED
                : Response::HTTP_OK;
        }

        $response->setStatusCode($statusCode);

        $event->setResponse($response);
    }
}
