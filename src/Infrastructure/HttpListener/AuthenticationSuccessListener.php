<?php

declare(strict_types=1);

namespace App\Infrastructure\HttpListener;

use App\Application\Security\UserAdapter;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Symfony\Component\HttpFoundation\Response;

final class AuthenticationSuccessListener
{
    public function __construct()
    {
    }

    public function handle(AuthenticationSuccessEvent $event): void
    {
        $data = $event->getData();
        $user = $event->getUser();

        if (!$user instanceof UserAdapter) {
            return;
        }

        $realUser = $user->unwrap();

        $data['id'] = (string) $realUser->getId();
        $data['name'] = $realUser->getName();
        $data['login'] = $realUser->getLogin();
        $data['email'] = $realUser->getEmail();

        $event->getResponse()->setStatusCode(Response::HTTP_OK);

        $event->setData($data);
    }
}
