<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\Movie;

use Symfony\Component\Console\Output\OutputInterface;

/**
 * @psalm-type MovieShape = array{
 *     title: string,
 *     description: string,
 *     link: string,
 *     image: null|string,
 *     pubDate: \DateTimeImmutable,
 * }
 */
interface MovieSourceServiceInterface
{
    /**
     * @return list<MovieShape>
     */
    public function loadAllMovies(OutputInterface $output): array;
}
