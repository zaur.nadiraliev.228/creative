<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\Movie;

use Klimick\Decode\Report\ErrorReport;
use RuntimeException;

final class ParsingMovieSourceException extends RuntimeException
{
    /**
     * @param non-empty-list<ErrorReport> $errors
     */
    public function __construct(public array $errors)
    {
        parent::__construct('Error occurred during parsing movies from source.');
    }
}
