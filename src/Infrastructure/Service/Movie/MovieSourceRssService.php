<?php

declare(strict_types=1);

namespace App\Infrastructure\Service\Movie;

use Feed;
use Klimick\Decode\Decoder as T;
use Klimick\Decode\Report\DefaultReporter;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use function array_merge;
use function Fp\Collection\map;
use function get_debug_type;
use function is_iterable;
use function is_string;
use function preg_match;
use function sprintf;

final class MovieSourceRssService implements MovieSourceServiceInterface
{
    private const IMG_TAG_SRC_PATTERN = '~img\s+src=\"(?<image>.*?)\"~';

    public function __construct(private string $source, private LoggerInterface $logger)
    {
    }

    public function loadAllMovies(OutputInterface $output): array
    {
        $progress = new ProgressBar($output);
        $xlmData = Feed::loadRss($this->source);

        $movieDecoder = T\shape(
            title: T\string(),
            description: T\string(),
            link: T\string(),
            pubDate: T\datetime(),
        );

        $moviesToParse = $xlmData->item ?? [];

        if (!is_iterable($moviesToParse)) {
            $this->logger->error('Unexpected xml data received.', ['data' => $xlmData]);

            throw new RuntimeException('Unexpected xml data received.');
        }

        $errors = [];
        $decodedMovies = [];

        /** @var mixed $item */
        foreach ($progress->iterate($moviesToParse) as $item) {
            $movieAsMixedArray = (array) $item;

            $decoded = T\decode($movieAsMixedArray, $movieDecoder)->get();

            if ($decoded instanceof T\Invalid) {
                $errors[] = $decoded;

                continue;
            }

            /**
             * @var $decoded T\Valid
             * @ignore-var
             */
            $movieShape = $decoded->value;

            $image = match (isset($movieAsMixedArray['content:encoded'])) {
                true => $this->parseImageFromEncodedHtmlContent($movieAsMixedArray['content:encoded']),
                false => null,
            };

            $decodedMovies[] = array_merge($movieShape, ['image' => $image]);
        }

        if ([] !== $errors) {
            $errors = map($errors, static fn (T\Invalid $invalid) => DefaultReporter::report($invalid));

            $this->logger->error(sprintf('There are %s occurred during parsing movies', count($errors)));

            throw new ParsingMovieSourceException($errors);
        }

        return $decodedMovies;
    }

    private function parseImageFromEncodedHtmlContent(mixed $contentEncoded): ?string
    {
        if (!is_string($contentEncoded)) {
            $this->logger->warning(sprintf('Encoded html content should be string, but %s received', get_debug_type($contentEncoded)));

            return null;
        }

        if (preg_match('' . self::IMG_TAG_SRC_PATTERN . '', $contentEncoded, $matches)) {
            return $matches['image'] ?? null;
        }

        $this->logger->warning('Encoded html content does not contain image url');

        return null;
    }
}
