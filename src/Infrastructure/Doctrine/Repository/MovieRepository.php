<?php

declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Repository;

use App\Entity\Movie\Movie;
use App\Entity\Movie\MovieRepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Fp\Functional\Option\Option;
use LogicException;
use Symfony\Component\Uid\Uuid;
use function Fp\Evidence\proveListOf;
use function Fp\Evidence\proveOf;
use function is_array;

final class MovieRepository implements MovieRepositoryInterface
{
    public function __construct(private EntityManagerInterface $em)
    {
    }

    public function save(Movie $movie): void
    {
        $this->em->persist($movie);
        $this->em->flush();
    }

    public function clearAll(): void
    {
        $this->em->createQueryBuilder()
            ->delete(Movie::class, 'm')
            ->getQuery()
            ->execute();
    }

    public function findAll(int $limit = null, int $offset = null): array
    {
        return Option
                ::do(function () use ($limit, $offset) {
                    /** @var mixed $queriedMovies */
                    $queriedMovies = $this->em->createQueryBuilder()
                        ->from(Movie::class, 'movie')
                        ->select('movie')
                        ->setFirstResult($offset)
                        ->setMaxResults($limit)
                        ->getQuery()
                        ->getResult();

                    $movies = yield self::proveArrayFromMixed($queriedMovies);

                    return yield proveListOf($movies, Movie::class);
                })
                ->get()
            ?? throw new LogicException('All data should be type of Movie'); // change exception please
    }

    public function findById(Uuid|string $movieId): ?Movie
    {
        return proveOf($this->em->find(Movie::class, (string) $movieId), Movie::class)->get()
            ?? throw new LogicException('Data should be type of Movie'); // change exception please
    }

    public function countAll(): int
    {
        return (int) $this->em->createQueryBuilder()
            ->from(Movie::class, 'movie')
            ->select('COUNT(movie)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param mixed $anyValue
     *
     * @return Option<array<array-key, mixed>>
     */
    private static function proveArrayFromMixed(mixed $anyValue): Option
    {
        return Option::fromNullable(is_array($anyValue) ? $anyValue : null);
    }
}
