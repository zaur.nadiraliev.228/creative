<?php

declare(strict_types=1);

namespace App\Infrastructure\Resolving;

use App\Application\Metadata\ResolveFromBody;
use function array_filter;
use function class_exists;
use Generator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\SerializerInterface;

final class FromBodyArgumentResolver implements ArgumentValueResolverInterface
{
    public function __construct(private SerializerInterface $serializer)
    {
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        $argumentTypehint = $argument->getType();

        if ($argumentTypehint === null || !class_exists($argumentTypehint)) {
            return false;
        }

        $neededAttributes = array_filter(
            $argument->getAttributes(),
            static fn (object $attribute) => $attribute instanceof ResolveFromBody,
        );

        return [] !== $neededAttributes;
    }

    public function resolve(Request $request, ArgumentMetadata $argument): Generator
    {
        $content = $request->getContent(asResource: false);

        /** @var class-string $argumentType */
        $argumentType = $argument->getType();

        yield $this->serializer->deserialize($content, $argumentType, format: 'json');
    }
}
