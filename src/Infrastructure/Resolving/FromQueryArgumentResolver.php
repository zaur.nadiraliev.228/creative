<?php

declare(strict_types=1);

namespace App\Infrastructure\Resolving;

use App\Application\Metadata\ResolveFromQuery;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use function array_filter;
use function array_values;
use function class_exists;
use Generator;
use function json_encode;
use const JSON_THROW_ON_ERROR;
use JsonException;
use LogicException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\SerializerInterface;

final class FromQueryArgumentResolver implements ArgumentValueResolverInterface
{
    public function __construct(private SerializerInterface $serializer)
    {
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        $argumentTypehint = $argument->getType();

        if ($argumentTypehint === null) {
            return false;
        }

        $fromQueryAttribute = self::findAttribute(array_values($argument->getAttributes()));

        return $fromQueryAttribute !== null;
    }

    /**
     * @throws JsonException
     */
    public function resolve(Request $request, ArgumentMetadata $argument): Generator
    {
        $data = $request->query->all();

        /** @var class-string $argumentTypehint */
        $argumentTypehint = $argument->getType();

        $attribute = self::findAttribute(array_values($argument->getAttributes()));

        if ($attribute === null) {
            throw new LogicException('Attribute should present, make sure method supports evaluated correctly.');
        }

        yield class_exists($argumentTypehint)
            ? $this->serializer->deserialize(
                json_encode($data, flags: JSON_THROW_ON_ERROR),
                type: $argumentTypehint,
                format: 'json',
                context: [AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true],
            )
            : $data[$attribute->name ?? $argument->getName()] ?? null;
    }

    /**
     * @param list<object> $attributes
     *
     * @return ResolveFromQuery|null
     */
    private static function findAttribute(array $attributes): ?ResolveFromQuery
    {
        $found = array_values(
            array_filter($attributes, static fn (object $attribute) => $attribute instanceof ResolveFromQuery)
        );

        return $found[0] ?? null;
    }
}
