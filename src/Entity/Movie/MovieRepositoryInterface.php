<?php

declare(strict_types=1);

namespace App\Entity\Movie;

use Symfony\Component\Uid\Uuid;

interface MovieRepositoryInterface
{
    public function save(Movie $movie): void;

    public function clearAll(): void;

    /**
     * @return list<Movie>
     */
    public function findAll(int $limit = null, int $offset = null): array;

    public function findById(Uuid|string $movieId): ?Movie;

    public function countAll(): int;
}
