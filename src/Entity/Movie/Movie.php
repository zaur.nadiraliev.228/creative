<?php

declare(strict_types=1);

namespace App\Entity\Movie;

use App\Entity\User\User;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity]
#[ORM\Table(name: 'movies')]
#[ORM\Index(fields: ['title'])]
class Movie
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    private Uuid $id;

    /**
     * @var Collection<array-key, User>
     */
    #[ORM\ManyToMany(targetEntity: User::class)]
    #[ORM\JoinTable('movies_users')]
    private Collection $usersWhoLiked;

    public function __construct(
        #[ORM\Column]
        private string $title,

        #[ORM\Column]
        private string $link,

        #[ORM\Column(type: Types::TEXT)]
        private string $description,

        #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
        private DateTimeImmutable $publicationDate,

        #[ORM\Column(nullable: true)]
        private ?string $image = null,
    ) {
        $this->id = Uuid::v4();
        $this->usersWhoLiked = new ArrayCollection();
    }

    /**
     * @psalm-mutation-free
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @psalm-mutation-free
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @psalm-mutation-free
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPublicationDate(): DateTimeImmutable
    {
        return $this->publicationDate;
    }

    /**
     * @psalm-mutation-free
     */
    public function getLink(): string
    {
        return $this->link;
    }

    /**
     * @psalm-mutation-free
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @return list<User>
     */
    public function getUsersWhoLiked(): array
    {
        return $this->usersWhoLiked->getValues();
    }

    /**
     * @psalm-mutation-free
     */
    public function getCountOfLikedUsers(): int
    {
        /** @psalm-suppress ImpureMethodCall */
        return $this->usersWhoLiked->count();
    }

    public function isUserAlreadyLiked(User $user): bool
    {
        return $this->usersWhoLiked->contains($user);
    }

    public function likeByUser(User $user): void
    {
        if ($this->isUserAlreadyLiked($user)) {
            return;
        }

        $this->usersWhoLiked->add($user);
    }

    public function unlikeByUser(User $user): void
    {
        $this->usersWhoLiked->removeElement($user);
    }
}
