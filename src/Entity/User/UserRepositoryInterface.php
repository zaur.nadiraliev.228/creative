<?php

declare(strict_types=1);

namespace App\Entity\User;

use Symfony\Component\Uid\Uuid;

interface UserRepositoryInterface
{
    public function save(User $user): void;

    public function findById(Uuid|string $id): ?User;

    public function findByLogin(string $login): ?User;

    public function findByEmail(string $email): ?User;
}
