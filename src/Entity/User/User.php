<?php

declare(strict_types=1);

namespace App\Entity\User;

use App\Entity\Movie\Movie;
use DateTimeImmutable;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;

#[ORM\Entity]
#[ORM\Table(name: 'users')]
#[ORM\UniqueConstraint(columns: ['login'])]
#[ORM\UniqueConstraint(columns: ['email'])]
class User
{
    #[ORM\Id]
    #[ORM\Column(type: 'uuid')]
    private Uuid $id;

    #[ORM\Column(type: Types::DATETIME_IMMUTABLE)]
    private DateTimeImmutable $createdAt;

    public function __construct(
        #[ORM\Column]
        private string $name,

        #[ORM\Column]
        private string $login,

        #[ORM\Column]
        private string $email,

        #[ORM\Column]
        private string $password,
    ) {
        $this->id = Uuid::v4();
        $this->createdAt = new DateTimeImmutable();
    }

    /**
     * @psalm-mutation-free
     */
    public function getId(): Uuid
    {
        return $this->id;
    }

    /**
     * @psalm-mutation-free
     */
    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @psalm-mutation-free
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @psalm-mutation-free
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @psalm-mutation-free
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @psalm-mutation-free
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function likeMovie(Movie $movie): void
    {
        $movie->likeByUser($this);
    }

    public function unlikeMovie(Movie $movie): void
    {
        $movie->unlikeByUser($this);
    }
}
